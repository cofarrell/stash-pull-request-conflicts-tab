package it.ru.yandex.stash.plugin.pull.conflicts;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.yandex.stash.plugin.pull.conflicts.PullRequestConflictsInspector;

import static org.junit.Assert.assertNotNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class PullRequestConflictsInspectorWiredTest {
    private final ApplicationProperties applicationProperties;
    private final PullRequestConflictsInspector pullRequestConflictsInspector;

    public PullRequestConflictsInspectorWiredTest(ApplicationProperties applicationProperties, PullRequestConflictsInspector pullRequestConflictsInspector) {
        this.applicationProperties = applicationProperties;
        this.pullRequestConflictsInspector = pullRequestConflictsInspector;
    }

    @Test
    public void testNotNull() {
        assertNotNull(pullRequestConflictsInspector);
    }
}