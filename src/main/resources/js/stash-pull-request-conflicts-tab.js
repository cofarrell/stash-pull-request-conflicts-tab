define('ru/pull-request/pull-request-conflicts',
    ['exports', 'page/pull-request/pull-request-view'],
    function(exports, pullRequestView) {

        // Registers the handler for a given feature, which in this case is a PR tab
        // This allows for purely client-side behaviour instead of having a full page refresh for each tab
        pullRequestView.registerHandler(
            // NOTE: This needs to match the web-item
            'stash.pull-request.nav.conflicts',
            // NOTE: This regex is very important and needs to match the servlet URL
            /^[^\?\#]*\/conflicts\/.*?\/pull-requests\/\d+/,
            // The defined handler from below which handles load/unload
            'ru/pull-request/pull-request-conflicts-view');
    }
);

// Look at the following package for more examples
// webapp/default/src/main/webapp/static/page/pull-request/view
define('ru/pull-request/pull-request-conflicts-view', [
    'jquery',
    'model/page-state'
], function (
    $,
    pageState
    ) {
    return {
        load : function(el) {
            // Calls into the client-side soy to render stuff
            // May need to do a REST call to retrieve data
            el.innerHTML = ru.yandex.conflicts.viewConflicts({});
        },
        unload : function(el) {
            // Maybe unload stuff here
        },
        keyboardShortcutContexts : ['conflicts']
    };
});

// Need to ensure the customer handler is registered
require('ru/pull-request/pull-request-conflicts');
