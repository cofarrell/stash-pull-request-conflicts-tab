package ru.yandex.stash.plugin.pull.conflicts;

import com.atlassian.sal.api.ApplicationProperties;

public class PullRequestConflictsInspectorImpl implements PullRequestConflictsInspector {
    private final ApplicationProperties applicationProperties;

    public PullRequestConflictsInspectorImpl(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }
}