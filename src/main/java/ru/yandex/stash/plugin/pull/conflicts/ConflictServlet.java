package ru.yandex.stash.plugin.pull.conflicts;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class ConflictServlet extends HttpServlet {

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final RepositoryService repositoryService;
    private final PullRequestService pullRequestService;
    private final StashAuthenticationContext authenticationContext;
    private final ApplicationPropertiesService propertiesService;

    public ConflictServlet(SoyTemplateRenderer soyTemplateRenderer, RepositoryService repositoryService, PullRequestService pullRequestService, StashAuthenticationContext authenticationContext, ApplicationPropertiesService propertiesService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.repositoryService = repositoryService;
        this.pullRequestService = pullRequestService;
        this.authenticationContext = authenticationContext;
        this.propertiesService = propertiesService;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String[] pathParts = pathInfo.substring(1).split("/");
        if (pathParts.length != 6) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        System.out.println(Arrays.asList(pathParts));
        String projectKey = pathParts[1];
        String repoSlug = pathParts[3];
        String prId = pathParts[5];
        Repository repo = repositoryService.findBySlug(projectKey, repoSlug);
        PullRequest pr = pullRequestService.findById(repo.getId(), Long.parseLong(prId));

        /**
         * ===============
         *    WARNING
         * ===============
         *
         * This soy template is _not_ considered part of our core API, and as such is subject to change at any point.
         * You have been warned!
         */
        render(resp, "stash.page.pullRequest.view",
                ImmutableMap.<String, Object>builder()
                        .put("repository", pr.getToRef().getRepository())
                        .put("pullRequest", pr)
                        .put("startingTabKey", "stash.pull-request.nav.conflicts")
                                // TODO These two values are wrong and shouldn't be hard-coded, but can't easily be loaded by a plugin
                        .put("maxChanges", 1000)
                        .put("isWatching", false)
                        .build());

    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), "com.atlassian.stash.stash-web-plugin:server-soy-templates", templateName, data,
                    getBuild());
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    /**
     * TODO This is currently only available to Spring MVC controllers, which sadly isn't possible in plugins.
     * <p/>
     * See {@code DefaultInjectedDataFactory.java} for all the missing arguments, these are just the bare minimum.
     *
     * @return
     */
    private ImmutableMap<String, Object> getBuild() {
        return ImmutableMap.<String, Object>builder()
                .put("instanceName", propertiesService.getDisplayName())
                .put("principal", authenticationContext.getCurrentUser())
                        // You don't _need_ this, but it should be there
                .put("timezone", -(propertiesService.getDefaultTimeZone().getOffset(System.currentTimeMillis()) / 1000 / 60))
                .build();
    }
}
