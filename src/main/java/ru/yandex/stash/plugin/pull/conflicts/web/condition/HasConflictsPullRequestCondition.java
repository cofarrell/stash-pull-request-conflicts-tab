package ru.yandex.stash.plugin.pull.conflicts.web.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.stash.content.Change;
import com.atlassian.stash.content.ChangeCallback;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Map;

/**
 * Condition for {@link com.atlassian.stash.pull.PullRequest} what it has conflicts.
 * Used in {@code stash.pull-request.nav} section.
 *
 * @author Alexey Efimov
 */
public class HasConflictsPullRequestCondition implements Condition {
    private final PullRequestService pullRequestService;

    public HasConflictsPullRequestCondition(PullRequestService pullRequestService) {
        this.pullRequestService = pullRequestService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        Repository repository = (Repository) context.get("repository");
        PullRequest pullRequest = (PullRequest) context.get("pullRequest");
        if (repository != null && pullRequest != null) {
            Integer repositoryId = repository.getId();
            Long pullRequestId = pullRequest.getId();
            if (repositoryId != null && pullRequestId != null) {
                HasConflictsCallback callback = new HasConflictsCallback();
                pullRequestService.streamChanges(repositoryId, pullRequestId, callback);
                return callback.hasConflicts();
            }
        }

        return false;
    }

    private static class HasConflictsCallback implements ChangeCallback {
        private boolean hasConflicts;

        @Override
        public boolean onChange(@Nonnull Change change) throws IOException {
            // As we found first conflict, then no other changes is needed
            this.hasConflicts |= change.getConflict() != null;
            return !hasConflicts;
        }

        @Override
        public void onEnd(boolean truncated) throws IOException {
        }

        @Override
        public void onStart() throws IOException {
        }

        public boolean hasConflicts() {
            return hasConflicts;
        }
    }
}
